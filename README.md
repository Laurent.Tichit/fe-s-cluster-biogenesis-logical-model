# Fe-S cluster biogenesis logical modeling scripts

This repository contains an application which is able to reproduce the heatmaps found in the article

Please follow the TOC for the requisites & tutorials.

[[_TOC_]]

## Pre-requisites

### Hardware requirements:

* 16 GB RAM 
* 4 cores CPU

### Software requirements:

This folder contains all the scripts used for analyzing the GINsim model from the article (`Model/Hammami2020.zginml`).

It needs a single dependency:

* Docker (https://docs.docker.com/get-started/)

Please refer to Docker documentation to download and install it on your computer.

## How to launch the Docker image

1. From the root of the repo, go to the docker folder:
```
cd Docker_GINsim_Heatmaps
```
2. Build the docker image through the dockerfile in the folder (this step takes a long time, but needs to be run only once):
```
docker build --tag testginsim .
```
3. Run the container:
```
docker run -d -p 3838:3838 --name heatmapgenerator testginsim
```
4. Open a webpage and go to `localhost:3838`


# How to use the application

In the application, the protocol is the following:

1. Load the model (with the example found in The_GINsim_Heatmap_Generator/Model/Hammami_2020.zginml)
2. Choose your mutation type (it only does all KO mutants or all ectopic mutants, no mix at the moment), and whether you want to perform the perturbations on all internal nodes or not.
3. Click on the bottom right button to launch the analysis (on the model delivered with the application, you have to wait 5-15 minutes)
4. Click on the "Simulation analysis" tab on the left, and choose your readouts. You can also select if you want the application to find mutants where the values of one or several model nodes are at 0.
5. Click on the button "Launch Analysis". The process may take 15-30 minutes depending of your RAM and processor. 


If you want to reproduce the results of the paper, then:

1. (Same as the step 1 described previously)
2. Choose all KO mutants
3. (same as step 3 described previously)
4. Choose Fe_free, H2O2, Isc, Suf, ErpA as readouts. Select Isc and Suf as phenotypes with mutants to search for.
5. (same as step 5 described previously)
